package ua.khpi.oop.isainkov07;

import java.util.ArrayList;
import java.util.Scanner;

public class Helper {

    public static void lets_go() {
        System.setProperty("console.encoding", "Cp866");
        Scanner sc = new Scanner(System.in);
        System.out.print("How many objects do you want to add?\n> ");
        int arraySize = sc.nextInt();
        ArrayList<File> person = new ArrayList<File>();
        File temp = null;
        for (int i = 0; i < arraySize; i++) {
            temp = new File();
            System.out.println((i + 1) + ".");
            temp.setName((enter_name()));
            temp.setConviction_dates(enter_conviction_dates());
            temp.setDate_of_last_imprisonment(enter_date_of_last_imprisonment());
            temp.setDate_of_last_release(enter_date_of_last_release());
            person.add(temp);
            temp = null;
        }
        showAccounts(person);
    }

    public static void showAccounts(ArrayList<File> person) {
        for (int i = 0; i < person.size(); i++) {
            person.get(i).print();
        }
    }

    public static String enter_name() {
        Scanner sc = new Scanner(System.in);
        System.out.print("Enter full name\n> ");
        String name = sc.nextLine();
        return name;
    }

    public static ArrayList enter_conviction_dates() {
        Scanner sc = new Scanner(System.in);
        ArrayList temp = new ArrayList();
        System.out.print("How many conviction dates?\n> ");
        int q = sc.nextInt();
        for(int i = 0; i < q; i++){
            Scanner st = new Scanner(System.in);
            System.out.print((i+1) + ". ");
            String str = new String();
            str = st.nextLine();
            temp.add(str);
        }
        return temp;
    }

    public static String enter_date_of_last_imprisonment() {
        Scanner sc = new Scanner(System.in);
        System.out.print("Enter date of last imprisonment\n> ");
        String imp = sc.nextLine();
        return imp;
    }

    public static String enter_date_of_last_release() {
        Scanner sc = new Scanner(System.in);
        System.out.print("Enter date of last release\n> ");
        String rel = sc.nextLine();
        return rel;
    }


}

