package ua.khpi.oop.isainkov08;

import java.util.ArrayList;

import java.util.Scanner;

public class Interface {

    public static void print_main_menu() {
        System.out.println("1.Enter data");
        System.out.println("2.Show current data");
        System.out.println("3.Clear all");
        System.out.println("4.Save or recover data");
        System.out.println("5.Exit");
    }


    public static void showPerson(ArrayList<Kard> person) {
        for (int i = 0; i < person.size(); i++) {
            person.get(i).print();
        }
    }

    public static String enter_name() {
        Scanner sc = new Scanner(System.in);
        System.out.print("Enter full name\n> ");
        String name = sc.nextLine();
        return name;
    }

    public static ArrayList enter_conviction_dates() {
        Scanner sc = new Scanner(System.in);
        ArrayList temp = new ArrayList();
        System.out.print("How many conviction dates?\n> ");
        int q = sc.nextInt();
        for(int i = 0; i < q; i++){
            Scanner st = new Scanner(System.in);
            System.out.print((i+1) + ". ");
            String str = new String();
            str = st.nextLine();
            temp.add(str);
        }
        return temp;
    }

    public static String enter_date_of_last_imprisonment() {
        Scanner sc = new Scanner(System.in);
        System.out.print("Enter date of last imprisonment\n> ");
        String imp = sc.nextLine();
        return imp;
    }

    public static String enter_date_of_last_release() {
        Scanner sc = new Scanner(System.in);
        System.out.print("Enter date of last release\n> ");
        String rel = sc.nextLine();
        return rel;
    }
    public static ArrayList<Kard> get_data() {
        @SuppressWarnings("resource")
        Scanner sc = new Scanner(System.in);

        String name = new String("1");
        String date_of_last_imprisonment;
        String date_of_last_release;
        ArrayList conviction_dates = new ArrayList();

        ArrayList<String> requirements = new ArrayList<String>();
        System.out.print("Enter the size of array:");
        int arraySize = sc.nextInt();
        ArrayList<Kard> person = new ArrayList<Kard>();


        for (int i = 0, j = 1; i < arraySize; i++, j++) {
            System.out.println("----------------" + (j) + "----------------");

            name = (enter_name());
            date_of_last_imprisonment = (enter_date_of_last_imprisonment());
            date_of_last_release = (enter_date_of_last_release());
            conviction_dates = enter_conviction_dates();

            String enter = "";


            person.add(new Kard(name, date_of_last_release, date_of_last_imprisonment, conviction_dates));
        }
        return person;
    }


    public static void printXmlMenu() {

        System.out.println("1. Save database");
        System.out.println("2. Recover database");

    }

    public static void printFileChooseMenu() {

        System.out.println("1. Continue choosing ");
        System.out.println("2. Create file in current directory");
        System.out.println("3. Return to previous directory");
        System.out.println("0. Exit");

    }

    public static void cls() {

        for (int i = 0; i < 100; i++) {

            System.out.println();

        }

    }
}
