﻿using System;
using System.Collections;

/// <summary>
/// Summary description for Class1
/// </summary>
namespace lab2
{
	public class Container : IEnumerable, IEnumerator
	{
		static int size = 2;
		StudentInfo[] students = new StudentInfo[size];
		int index = -1;
		int currentSize = 0;
		public IEnumerator GetEnumerator() {
			return this;
		}
		public void Reset()
		{
			index = -1;
		}
		public bool MoveNext()
		{
			if(index == students.Length-1) {
					Reset(); ;
					return false;
			}
			index++;
			return true;
		}
		public object Current
		{
			get
			{
				return students[index];
			}
		}
		int NewSize()
		{
			currentSize = 0;
			for (int i = 0; i < students.Length; i++)
			{
				if (students[i]!=null)
				{
					currentSize++;
				}
			}
			return currentSize;
		}
		public void Add(StudentInfo student)
		{
			currentSize = NewSize();
			Array.Resize(ref students, size);
			students[currentSize] = student;
		}
		public void Serach(int number)
		{
			students[number].showData();
		}
		public void showData()
		{
			foreach(var student in students)
			{
				student.showData();
			}
		}
	}
}