package ua.khpi.oop.isainkov04;

import ua.khpi.oop.isainkov03.functional;

import java.util.Scanner;

public class Lab04 {

    public static void main(String[] args) {
        int choice;
        Scanner inp = new Scanner(System.in);
        String sg = new String();
        System.out.print("--------- MENU --------- \n 1. Input data \n 2. Output data \n 3. Edit data \n 4. About programm \n 0. Exit \n \n ");
        System.out.print("Eneter your choice -> ");
        choice = inp.nextInt();
        inp.reset();
        while (choice != 0) {
            switch (choice) {
                case 1: {
                    Scanner in = new Scanner(System.in);
                    System.out.print("Input here: ");
                    sg = in.nextLine();
                    System.out.print("... Reading complete! ...");
                    break;
                }
                case 2: {
                    System.out.println("DATA : " + sg);
                    break;
                }
                case 3: {
                    sg = functional.replace(sg);
                    System.out.println("... Editing complete! ...");
                    break;
                }
                case 4: {

                    System.out.println("-------------------------------- \n Version: Realise 1.0 \n Author: Isainkov Ilya \n About: The program removes all characters from the text except spaces, \n which are not letters. Replaces repeated repeats with single spaces. \n There are one space between the letter sequences where the \n punctuation marks are (\"a, b, c\" -> \"a b c\"). \n -------------------------------- \n");
                    break;
                }
                default: {
                    System.out.println("Wrong choice! Try again.");
                    break;
                }
            }

            System.out.print("\nEneter your choice ->");
            choice = inp.nextInt();

        }
        inp.close();
    }
}




