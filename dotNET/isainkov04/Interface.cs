﻿using System;
using System.IO;
using System.Text;

namespace isainkov04
{
    public class Interface
    {
        private Container<StudentInfo> _list;
        public void Start()
        {
            _list = new Container<StudentInfo>();
            char choice = ' ';

            StudentInfo tmpStudent;

            while (choice != '0')
            {

                printMenu();
                Console.Write("Your choice: ");
                choice = DataInput.InputChar();

                switch (choice)
                {
                    case '1':
                        Console.Clear();
                        tmpStudent = DataInput.InputStudent();
                        _list.Add(tmpStudent);
                        Console.Clear();
                        break;
                    case '2':
                        Console.Clear();
                        ShowAll();
                        Console.ReadLine();
                        Console.Clear();
                        break;
                    case '3':
                        Console.Clear();
                        printByIndex();
                        Console.ReadLine();
                        Console.Clear();
                        break;
                    case '4':
                        int a;
                        Console.Clear();
                        Console.Write("Set index: ");
                        a = Convert.ToInt32(Console.ReadLine());
                        RemoveByIndex(a);
                        Console.ReadLine();
                        Console.Clear();
                        break;
                    case '5':
                        Console.Clear();
                        WriteToFile();
                        Console.WriteLine("Info was written");
                        Console.ReadLine();
                        Console.Clear();
                        break;
                    case '6':
                        Console.Clear();
                        ReadFromFile();
                        Console.WriteLine("List was downloaded");
                        Console.ReadLine();
                        Console.Clear();
                        break;
                    case '7':
                        Console.Clear();
                        RedactStudent();
                        Console.ReadLine();
                        Console.Clear();
                        break;
                    case '8':
                        Console.Clear();
                        int index;
                        Console.Write("Choose the index of student: ");
                        index = Convert.ToInt32(Console.ReadLine());
                        if (index > _list.Count() || index < 0)
                        {
                            Console.Write("Invalid index ");
                        }
                        else
                        {
                            ShowStudentSpecialization(index);
                        }
                        Console.ReadLine();
                        Console.Clear();
                        break;
                    case '9':
                        Console.Clear();
                        int i;
                        Console.Write("Choose the index of student: ");
                        i = Convert.ToInt32(Console.ReadLine());
                        if (i > _list.Count() || i < 0)
                        {
                            Console.Write("Invalid index ");
                        }
                        else
                        {
                            ShowCourseAndSemestr(i);
                        }
                        Console.ReadLine();
                        Console.Clear();
                        break;
                }
            }
        }

        private static void printMenu()
        {
            Console.WriteLine("--------- Menu ---------\n");
            Console.WriteLine("1 - Add new student");
            Console.WriteLine("2 - Print all data");
            Console.WriteLine("3 - Print by index");
            Console.WriteLine("4 - Remove by index");
            Console.WriteLine("5 - Write to file");
            Console.WriteLine("6 - Read from file");
            Console.WriteLine("7 - Change info");
            Console.WriteLine("8 - Show student specialization");
            Console.WriteLine("9 - Show course and semestr");
            Console.WriteLine("0 - Exit");
            Console.WriteLine("\n------ END of Menu ------\n");
        }

        public void ShowAll()
        {
            int index = 0;
            foreach (var i in _list)
            {
                Console.WriteLine("-------- Student #" + index + " --------\n");
                Console.WriteLine(i);
                index++;
            }
        }

        public void printByIndex()
        {
            int index;
            Console.WriteLine("Set index: ");
            index = Convert.ToInt32(Console.ReadLine());
            if (index >= _list.Count() || index < 0)
            {
                Console.WriteLine("Invalid index");
                return;
            }
            Console.WriteLine(_list[index]);
        }

        void RemoveByIndex(int index)
        {
            _list.Remove(index);
        }
        public void WriteToFile()
        {
            Console.WriteLine("Enter path of file (Format: C:\\Users\\UserName\\example.txt). Defoult: D:\\University\\dotNET\\isainkov03\\data.txt ");
            string path;
            path = DataInput.InputString();
            if (path == "defoult" || path == "Defoult")
            {
                path = @"D:\University\dotNET\isainkov03\data.txt";
            }
            try
            {
                using (StreamWriter sw = new StreamWriter(path, false, System.Text.Encoding.Default))
                {
                    foreach (var student in _list)
                    {
                        sw.WriteLine(student.LastName + '/' + student.FirstName + '/' + student.Fathersname + '/' +
                            student.Birthday.ToString("d") + '/' + student.DateEnter.ToString("d") + '/' + student.NameOfGroup + '/' +
                            student.IndexOfGroup + '/' + student.Specialization + '/' + student.Performance);
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }

        public void ReadFromFile()
        {
            string LastName;
            string FirstName;
            string Fathersname;
            DateTime Birthday;
            DateTime DateEnter;
            string NameOfGroup;
            char IndexOfGroup;
            string Specialization;
            double Performance;
            Console.WriteLine("Enter path of file (Format: C:\\Users\\UserName\\example.txt). Defoult: D:\\University\\dotNET\\isainkov03\\data.txt ");
            string path;
            path = DataInput.InputString();
            if (path == "defoult" || path == "Defoult")
            {
                path = @"D:\University\dotNET\isainkov03\data.txt";
            }
            try
            {
                using (StreamReader sr = new StreamReader(path, System.Text.Encoding.Default))
                {
                    string line;
                    while ((line = sr.ReadLine()) != null)
                    {
                        var infoStudent = line.Split("/");

                        LastName = infoStudent[0];
                        FirstName = infoStudent[1];
                        Fathersname = infoStudent[2];
                        Birthday = DateTime.Parse(infoStudent[3]);
                        DateEnter = DateTime.Parse(infoStudent[4]);
                        NameOfGroup = infoStudent[5];
                        IndexOfGroup = char.Parse(infoStudent[6]);
                        Specialization = infoStudent[7];
                        Performance = int.Parse(infoStudent[8]);
                        var obj = new StudentInfo(NameOfGroup, IndexOfGroup, Specialization, FirstName, LastName, Fathersname, Birthday, DateEnter, Performance);
                        _list.Add(obj);

                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }

        public void RedactStudent()
        {
            if (_list.Count() == 0)
            {
                Console.WriteLine("The list of students is empty");
                Console.ReadLine();
                return;
            }
            string selection;
            var number = 0;
            var status = false;
            int index = 0;
            while (status == false)
            {
                Console.Write("Choose the index of student: ");
                index = Convert.ToInt32(Console.ReadLine());
                if (index < _list.Count() && index >= 0)
                {
                    status = true;
                }
            }

            Console.WriteLine("1 - exit");
            Console.WriteLine("You can change: ");
            Console.WriteLine("2 - Last name");
            Console.WriteLine("3 - First name");
            Console.WriteLine("4 - Father`s name");
            Console.WriteLine("5 - Birthday");
            Console.WriteLine("6 - Date of enter");
            Console.WriteLine("7 - Group");
            Console.WriteLine("8 - Group index");
            Console.WriteLine("9 - Specialization");
            Console.WriteLine("10 - Performance");
            selection = Console.ReadLine();
            int day;
            int month;
            int year;

            if (int.TryParse(selection, out number))
            {

                switch (number)
                {
                    case 1:

                        return;

                    case 2:

                        Console.Write("Last name: ");
                        string newLastName = DataInput.InputName();
                        _list[index].LastName = newLastName;
                        break;

                    case 3:

                        Console.Write("First name: ");
                        string newName = DataInput.InputName();
                        _list[index].FirstName = newName;

                        break;

                    case 4:

                        Console.Write("Father`s name: ");
                        string newFName = DataInput.InputName();
                        _list[index].Fathersname = newFName;
                        break;

                    case 5:

                        Console.Write("BirthDay: ");
                        day = DataInput.InputInt();
                        Console.Write("BirthMonth: ");
                        month = DataInput.InputInt();
                        Console.Write("BirthYear: ");
                        year = DataInput.InputInt();
                        DateTime newBd = new DateTime(year, month, day);
                        _list[index].Birthday = newBd;
                        break;

                    case 6:

                        Console.Write("Date of enter: ");
                        day = DataInput.InputInt();
                        Console.Write("Month of enter: ");
                        month = DataInput.InputInt();
                        Console.Write("Year of enter: ");
                        year = DataInput.InputInt();
                        DateTime newDateEnter = new DateTime(year, month, day);
                        _list[index].DateEnter = newDateEnter;
                        break;

                    case 7:

                        Console.Write("Group: ");
                        string newNameGroup = DataInput.InputString();
                        _list[index].NameOfGroup = newNameGroup;
                        break;

                    case 8:

                        Console.Write("Group index: ");
                        char newGroupIndex = DataInput.InputChar();
                        _list[index].IndexOfGroup = newGroupIndex;
                        break;

                    case 9:

                        Console.Write("Specialization: ");
                        string newSpec = DataInput.InputString();
                        _list[index].Specialization = newSpec;
                        break;

                    case 10:

                        Console.Write("Performance: ");
                        double newPerf = DataInput.inputDouble();
                        _list[index].Performance = newPerf;
                        break;
                }

            }
        }

        public void ShowStudentSpecialization(int index)
        {
            if (index >= _list.Count() || index < 0)
            {
                Console.WriteLine("Invalid index");
                return;
            }
            var info = new StringBuilder();
            info.Append($"Specialization: {_list[index].Specialization}\nName of group: {_list[index].NameOfGroup}\n" +
                $"Enter year: {_list[index].DateEnter.Year}\nGroup index: {_list[index].IndexOfGroup}\n");
            Console.Write(info);
        }

        public void ShowCourseAndSemestr(int index)
        {
            if (index >= _list.Count() || index < 0)
            {
                Console.WriteLine("Invalid index");
                return;
            }
            var nowDate = DateTime.Now;
            int semester;

            int course;

            if (nowDate.Month >= 7)
            {
                course = nowDate.Year - _list[index].DateEnter.Year + 1;
            }
            else
            {
                course = nowDate.Year - _list[index].DateEnter.Year;
            }

            if (nowDate.Month >= 7)
            {
                semester = course * 2 - 1;
            }
            else
            {
                semester = course * 2;
            }
            var info = new StringBuilder();
            info.Append($"StudentInfo: {_list[index].LastName} {_list[index].FirstName} {_list[index].Fathersname}\n" +
                $"Course {course}, semester {semester}\n");
            Console.Write(info);
        }

    }
}
