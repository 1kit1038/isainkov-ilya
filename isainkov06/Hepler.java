package ua.khpi.oop.isainkov06;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.Scanner;

//import ua.khpi.oop.kononenko05.*; //Импорт чужого класса

public class Hepler {

    static My_iter_class list = new My_iter_class();

    public static void start() throws IOException, ClassNotFoundException {
        int choice;
        Scanner temp = new Scanner(System.in);

        do {
            Hepler.print_main_menu();
            choice = temp.nextInt();
            switch (choice) {
                case 1:
                    Hepler.get_data();
                    break;
                case 2:
                    Hepler.print_arr();
                    break;
                case 3:
                    list.sortArray();
                    break;
                case 4:
                    Hepler.addElement();
                    break;
                case 5:
                    Hepler.removeElement();
                    break;
                case 6:
                    Scanner tempStr = new Scanner(System.in);
                    System.out.println("Which string do you need?");
                    String c = new String();
                    c = tempStr.nextLine();
                    System.out.println(list.indexOf(c));
                    break;
                case 7:
                    list.clear();
                    break;
                case 8:
                    Hepler.saveContainer();
                    break;
                case 9:
                    Hepler.recoverContainer();
                    break;
                case 10:
                    String temp2 = list.getByIndex(0);
                    Hepler.replace(temp2);
                    break;
                case 11:
                    //Helper a = new Helper(); //Использование чужого класса
                    // a.Parse(q.get(choice-1));
                    // a.Print_result();
                    //Helper.Print_info();
                    System.out.println("Press Any Key To Continue...");
                    new java.util.Scanner(System.in).nextLine();

                    break;
                default:
                    break;
            }
        } while (choice != 11);
    }


    static boolean Vowel(char c) {
        switch (c) {
            case 'A':
            case 'a':
            case 'e':
            case 'i':
            case 'o':
            case 'u':
            case 'y':
                return true;
            default:
                return false;
        }
    }

    static public String replace(String arg) {
        char[] sourceCharArray = arg.toCharArray();
        StringBuilder strFinal = new StringBuilder();

        outer:
        for (int i = 0; i < arg.length(); i++) {
            if ((sourceCharArray[i] >= 65 && sourceCharArray[i] <= 90) || (sourceCharArray[i] >= 97 && sourceCharArray[i] <= 122) || sourceCharArray[i] == ' ' || sourceCharArray[i] == 46 || sourceCharArray[i] == 44) {
                if (sourceCharArray[i] == ' ')
                    if (sourceCharArray [i + 1] == ' ') continue outer;
                if (sourceCharArray[i] == 46 || sourceCharArray[i] == 44) {
                    strFinal.append(' ').toString();
                } else strFinal.append(sourceCharArray[i]).toString();


            }
        }
        String str = strFinal.toString();

        return str;
    }

    public static void print_main_menu() {
        System.out.println("1.Enter data");
        System.out.println("2.Show data");
        System.out.println("3.Sort by alphabet");
        System.out.println("4.Add data");
        System.out.println("5.Remove element");
        System.out.println("6.Find index of element");
        System.out.println("7.Clear");
        System.out.println("8.Save data");
        System.out.println("9.Recover data");
        System.out.println("10.Format Text");
        System.out.println("11.Exit");
    }

    public static void print_arr() {
        System.out.println("Your array is:");
        for (String str : list) {
            System.out.println("  " + str);
        }
    }

    public static void get_data() {
        Scanner sc = new Scanner(System.in);
        System.out.println("Enter the text: ");
        String current = sc.nextLine();
        while (!current.equals("")) {
            list.add(new String(current));
            current = sc.nextLine();
        }
    }

    static public void addElement() {
        Scanner sc = new Scanner(System.in);
        System.out.println("Enter the text:");
        String current = sc.nextLine();
        list.add(current);
    }

    static public void removeElement() {
        Scanner sc = new Scanner(System.in);
        System.out.println("Enter the text:");
        String current = sc.nextLine();
        if (!list.remove(current)) System.out.println("Cannot find the string :(");
        ;
    }

    static public void saveContainer() throws IOException {
        FileOutputStream outputStream = new FileOutputStream("C:\\Users\\Isainkov.offline\\isainkov-ilya\\src\\ua\\khpi\\oop\\isainkov06\\savedContainer.txt");
        ObjectOutputStream objectOutputStream = new ObjectOutputStream(outputStream);
        objectOutputStream.writeObject(list);
        objectOutputStream.close();
    }

    static public void recoverContainer() throws IOException, ClassNotFoundException {
        FileInputStream fileInputStream = new FileInputStream("C:\\Users\\Isainkov.offline\\isainkov-ilya\\src\\ua\\khpi\\oop\\isainkov06\\savedContainer.txt");
        ObjectInputStream objectInputStream = new ObjectInputStream(fileInputStream);
        list = (My_iter_class) objectInputStream.readObject();
    }

}

