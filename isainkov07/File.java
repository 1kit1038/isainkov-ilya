package ua.khpi.oop.isainkov07;

import java.util.ArrayList;

public class File {

    private String name;
    private ArrayList conviction_dates = new ArrayList();
    private String date_of_last_imprisonment;
    private String date_of_last_release;

    public File() {

    }

    public String getDate_of_last_imprisonment() {
        return date_of_last_imprisonment;
    }

    public void setDate_of_last_imprisonment(String date_of_last_imprisonment) {
        this.date_of_last_imprisonment = date_of_last_imprisonment;
    }

    public String getDate_of_last_release() {
        return date_of_last_release;
    }

    public void setDate_of_last_release(String date_of_last_release) {
        this.date_of_last_release = date_of_last_release;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ArrayList getConviction_dates() {
        return conviction_dates;
    }

    public void setConviction_dates(ArrayList conviction_dates) {
        this.conviction_dates = conviction_dates;
    }

    public void print() {
        System.out.println("------------ Information -------------- ");
        System.out.print("Name: ");
        System.out.println(this.name);
        System.out.println("Conviction_dates: ");
        for (int i = 0; i < this.conviction_dates.size(); i++) {
            System.out.print((i + 1) + ". ");
            System.out.println(this.conviction_dates.get(i));
        }
        System.out.print("Date of last imprisonment: ");
        System.out.println(this.date_of_last_imprisonment);
        System.out.print("Date of last release: ");
        System.out.println(this.date_of_last_release);
    }
}