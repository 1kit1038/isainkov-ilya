package ua.khpi.oop.isainkov02;


public class lab02 {

    static int rand() {
        int min = 100000;
        int max = 999999;
        int diff = max - min;
        java.util.Random random = new java.util.Random();
        int temp = random.nextInt(diff + 1);
        temp += min;

        return temp;
    }

    static void check(int arg) {
        if (arg / 1000000 != 0 || arg / 10 == 0 || arg / 100 == 0 || arg / 1000 == 0 || arg / 10000 == 0 || arg / 100000 == 0) {
            System.out.println("!!! You have entered NOT a six-digit number !!!");
            System.exit(0);
        }
    }

    static boolean comparison(int arg_num) {
        int counter = 0;
        int amount1 = 0;
        int amount2 = 0;
        boolean result = false;

        while (counter != 3) {
            amount1 += arg_num % 10;
            arg_num /= 10;
            counter++;
        }
        counter = 0;
        while (counter != 3) {
            amount2 += arg_num % 10;
            arg_num /= 10;
            counter++;
        }

        if (amount1 == amount2) result = true;
        return result;
    }


    public static void main(String[] args) {

        int num;
        for (int i = 0; i < 15; i++) {
            System.out.print((i+1) + ".\t" + (num = rand())+ "\t");
            check(num);
            System.out.println(comparison(num));
        }


    }

}
