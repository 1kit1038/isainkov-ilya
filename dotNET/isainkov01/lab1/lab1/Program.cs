﻿using System;

namespace lab1
{
    class Program
    {
        static void Main(string[] args)
        {
            var Student = new StudentInfo();
            bool key = true;
            Console.WriteLine($"Enter or display student data? (e/d)");

            while (key)
            {
                string value = Console.ReadLine();
                if (value == "e" || value == "E")
                {
                    Console.WriteLine($"Name: ");
                    Student.Name = Console.ReadLine();
                    Console.WriteLine($"Date Of Birthday: ");
                    Student.DateOfBirthday = Console.ReadLine();
                    Console.WriteLine($"Date Of Receipt: ");
                    Student.DateOfReceipt = Console.ReadLine();
                    Console.WriteLine($"Group Id: ");
                    Student.GroupId = Console.ReadLine();
                    Console.WriteLine($"Speciality: ");
                    Student.Speciality = Console.ReadLine();
                    Console.WriteLine($"Performance: ");
                    Student.Performance = Console.ReadLine();
                    Student.showData();
                }
                else if (value == "D" || value == "d")
                {
                    key = false;
                    Student.showData();
                }
                else break;
            }
            
        }
    }
}
