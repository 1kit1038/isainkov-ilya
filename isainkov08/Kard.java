package ua.khpi.oop.isainkov08;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;

public class Kard implements Serializable {

    private static final long serialVersionUID = 1L;

    private String name;
    private ArrayList conviction_dates = new ArrayList();
    private String date_of_last_imprisonment;
    private String date_of_last_release;

    public Kard() {
name = new String();
date_of_last_imprisonment = new String();
date_of_last_release = new String();
    }

    public Kard(String name, String date_of_last_release, String date_of_last_imprisonment, ArrayList conviction_dates){
        setDate_of_last_imprisonment(date_of_last_imprisonment);
        setDate_of_last_release(date_of_last_release);
        setName(name);
        setConviction_dates(conviction_dates);
    }

    public String getDate_of_last_imprisonment() {
        return date_of_last_imprisonment;
    }

    public void setDate_of_last_imprisonment(String date_of_last_imprisonment) {
        this.date_of_last_imprisonment = date_of_last_imprisonment;
    }

    public String getDate_of_last_release() {
        return date_of_last_release;
    }

    public void setDate_of_last_release(String date_of_last_release) {
        this.date_of_last_release = date_of_last_release;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ArrayList getConviction_dates() {
        return conviction_dates;
    }

    public int getConviction_dates_size(){
        return this.conviction_dates.size();
    }

    public void setConviction_dates(ArrayList conviction_dates) {
        this.conviction_dates = conviction_dates;
    }

    public void print() {
        System.out.println("------------ Information -------------- ");
        System.out.print("Name: ");
        System.out.println(this.name);
        System.out.println("Conviction_dates: ");
        for (int i = 0; i < this.conviction_dates.size(); i++) {
            System.out.print((i + 1) + ". ");
            System.out.println(this.conviction_dates.get(i));
        }
        System.out.print("Date of last imprisonment: ");
        System.out.println(this.date_of_last_imprisonment);
        System.out.print("Date of last release: ");
        System.out.println(this.date_of_last_release);
    }
}


