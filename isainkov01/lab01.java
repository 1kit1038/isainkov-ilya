package ua.khpi.oop.isainkov01;


public class lab01 {

    static int number_of_odds(long n) {

        long units;
        units = n % 10;
        int number = 0;
        boolean is_odd = ((units % 2) != 0);
        if (is_odd)
            number = number + 1;
        long number_without_units;
        number_without_units = n / 10;
        if (number_without_units == 0)
            return number;
        return number + number_of_odds(number_without_units);
    }

    static int q_of_num(long num) {
        int count = 1;
        while ((num /= 10) > 0) count++;
        return count;
    }

    static void even_and_odd(long _num) {
        long num_amount = q_of_num(_num);
        long num_odd = number_of_odds(_num);
        long num_even = num_amount - num_odd;
        System.out.println("Amount of even = " + num_even + "; Amount of odd = " + num_odd);
    }


    private static void binaryform(long number) {
        int i = 0;
        int t = 0;
        while (number != 0) {
            long temp = (number >> 1);
            temp = number - (temp << 1);

            if (temp == 1) {
                i++;
            } else {

                if (temp == 0) t++;
            }
            System.out.print(temp);

            number >>= 1;
        }
        System.out.println();
        System.out.println("Even = " + i + "; Null = " + t);
    }


    public static void main(String[] args) {
        int gradebook_number = 0x84F;
        long telephone_number = 380991758456l;
        byte last_number = 0b111000;
        short last_4_number = 020410;
        byte ost = (5 % 26) + 1;
        char let = 'F';
        System.out.println("gradebook_number = " + gradebook_number);
        even_and_odd(gradebook_number);
        System.out.println();

        System.out.println("telephone_number = " + telephone_number);
        even_and_odd(telephone_number);
        System.out.println();

        System.out.println("last_number = " + last_number);
        even_and_odd(last_number);
        System.out.println();

        System.out.println("last_4_number = " + last_4_number);
        even_and_odd(last_4_number);
        System.out.println();

        System.out.println("ost = " + ost);
        even_and_odd(ost);
        System.out.println();

        System.out.println("gradebook_number = " + gradebook_number);
        binaryform(gradebook_number);
        System.out.println();

        System.out.println("telephone_number = " + telephone_number);
        binaryform(telephone_number);
        System.out.println();

        System.out.println("last_number = " + last_number);
        binaryform(last_number);
        System.out.println();

        System.out.println("last_4_number = " + last_4_number);
        binaryform(last_4_number);
        System.out.println();

        System.out.println("ost = " + ost);
        binaryform(ost);
        System.out.println();

    }
}
